<?php
/*
 * @file
 * The plugin for views, which dictates and molds the input and output of 
 * the plugin.
 *
 * Few options are avilable to the user, as the Role Table was designed to be
 * used explicitly with roles as the collecting column. Making other fields
 * available for that use would mean different/more complex queries, which was
 * not the original intention of this plugin. Row Grouping has been removed from
 * form as well, as it has a tendency to break the themed output.
 */
class role_table_plugin_style_role_table extends views_plugin_style {
	/*
	 * Defines the positive and negative match options.
	 */
	function option_definition() {
		$options['matchPos'] = array();
		$options['matchNeg'] = array();
		
		return $options;
	}
	
	/**
	 * The two avilable forms; for positive and negative matches. These characters can be changed in the view menu.
	 */
	function options_form(&$form, &$form_state) {
		$form['matchPos'] = array(
			'#type' => 'textfield',
			'#title' => t('Positive Match'),
			'#default_value' => 'X',
			'#description' => 'This character or string will be displayed in the table when users\'s roles match an item on the role list.',
		);
		$form['matchNeg'] = array(
			'#type' => 'textfield',
			'#title' => t('Negative Match'),
			'#default_value' => '',
			'#description' => 'This character or string will be displayed in the table when users\'s roles do not match an item on the role list.',
		);
	}

	/*
	 * Grabs rendered field data and roles list, then cross-checks the roles
	 * list with users' role lists for matches. Matches are identified by an
	 * 'X'.
	 * 
	 * If interested in changing the identifiers for negative or positive
	 * matches between the users' roles and the role list, variables
	 *
	 * The array containing all the data is themed through theme.inc and
	 * returned as $output.
	 */
	function render() {
		$this->render_fields($this->view->result);
		$field_data['rendered'] = $this->rendered_fields;
		
		$result = db_query("SELECT r.name FROM {role} r ORDER BY r.name");
		while ($role = db_fetch_object($result)) {
			$table['header'][] = check_plain($role->name);
		}
		
		foreach ($field_data['rendered'] as $key => $user) {
			foreach ($table['header'] as $role) {
				if (strstr($user['rid'], $role)) {
				$temp[$role] = $this->options['matchPos'];
				} elseif (!isset($var[$role])) {
				$temp[$role] = $this->options['matchNeg'];
				}
			}
			$field_data['rendered'][$key]['rid'] = $temp;
			unset($temp);
		}
		
		array_unshift($table['header'], '');
		$output .= theme('role_table_form', $table['header'], $field_data['rendered']);
		return $output;
	}
}