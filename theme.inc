<?php
/*
 * @file
 * Data is plugged into the theme function, which outputs a finished table.
 *
 * The majority of the structure for the table has already been completed prior
 * to this function. The array $table is the central variable which holds all
 * table data. This function adds tags to the array, and concatenates it into
 * an html table. Styles for the table can be changed in the role_table_style.css
 * file, but should not be edited too much, as certain changes can have
 * undesirable effects on the table.
 */
function theme_role_table_form($header, $table) {

	drupal_add_js(drupal_get_path('module', 'role_table') . '/role_table.js');
	drupal_add_css(drupal_get_path('module', 'role_table') . '/role_table_style.css', 'theme', 'all', FALSE);

	$columns = count($header);
	foreach ($table as $dataArray) { 
		$name = $dataArray['name'];
		$rid = $dataArray['rid'];
		unset($dataArray['name']);
		unset($dataArray['rid']);
		$extra[] = $dataArray;
		$core[] = array('name' => $name, 'rid' => $rid, 'extra' => $dataArray);
	}
	$output = '<table>';
	$output .= '<tr>';
	foreach ($header as $roles) {
		$output .= '<td class="head">' . $roles . '</td>';
	}

	//$m used to identify name/desc relationships and increments with each new row
	$incVariable = 1;
	$output .= '</tr>';
	foreach ($core as $key => $data) {
		$output .= '<tr>';
		$output .= '<td><div onclick="slider(\'#panel' . $incVariable . '\')">' . $data['name'] . '</div></td>';
		foreach ($data['rid'] as $oi) {
			if (!empty($oi)) {
				$output .= '<td class="tdx">' . $oi . '</td>';
			} else {
				$output .= '<td>' . $oi . '</td>';
			}
		}
		$output .= '</tr><tr><td class="desc" colspan="' . $columns . '"><div class="panel" id="panel' . $incVariable . '">';
		foreach ($data['extra'] as $keyo => $io) {
			$output .= '<span class="str">' . $keyo . ': </span>' . $io . ' | ';
		}
		$output .= '</div></td></tr>';
		$incVariable++;
	}

	$output .= '</table>';
	return $output;
}